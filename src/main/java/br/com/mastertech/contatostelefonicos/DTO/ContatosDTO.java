package br.com.mastertech.contatostelefonicos.DTO;

import java.util.List;

public class ContatosDTO {

    private String usuario;

    private List<Telefone> telefones;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }
}
