package br.com.mastertech.contatostelefonicos.DTO;

public class Telefone {

    private String numeroTelefone;

    public String getNumeroTelefone() {
        return numeroTelefone;
    }

    public void setNumeroTelefone(String numeroTelefone) {
        this.numeroTelefone = numeroTelefone;
    }
}
