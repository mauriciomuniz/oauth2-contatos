package br.com.mastertech.contatostelefonicos.controller;

import br.com.mastertech.contatostelefonicos.DTO.ContatosDTO;
import br.com.mastertech.contatostelefonicos.model.Contato;
import br.com.mastertech.contatostelefonicos.security.Usuario;
import br.com.mastertech.contatostelefonicos.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping("/contato")
    @ResponseStatus(HttpStatus.CREATED)
    public Contato criarContato(@RequestBody @Valid Contato contato, @AuthenticationPrincipal Usuario usuario) {
        Contato contatoRetorno = contatoService.criarContato(contato, usuario);
        return contatoRetorno;
    }

    @GetMapping("/contatos")
    @ResponseStatus(HttpStatus.OK)
    public ContatosDTO listarContatos(@AuthenticationPrincipal Usuario usuario) {
        return contatoService.listarContatosPorUsuario(usuario);
    }
}
