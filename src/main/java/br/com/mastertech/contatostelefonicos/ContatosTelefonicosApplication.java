package br.com.mastertech.contatostelefonicos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContatosTelefonicosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContatosTelefonicosApplication.class, args);
	}

}
