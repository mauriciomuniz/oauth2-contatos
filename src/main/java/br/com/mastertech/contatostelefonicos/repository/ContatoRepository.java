package br.com.mastertech.contatostelefonicos.repository;

import br.com.mastertech.contatostelefonicos.model.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    List<Contato> findAllByUsuarioId(Integer id);
}
