package br.com.mastertech.contatostelefonicos.service;

import br.com.mastertech.contatostelefonicos.DTO.ContatosDTO;
import br.com.mastertech.contatostelefonicos.DTO.Telefone;
import br.com.mastertech.contatostelefonicos.model.Contato;
import br.com.mastertech.contatostelefonicos.repository.ContatoRepository;
import br.com.mastertech.contatostelefonicos.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato criarContato(Contato contato, @AuthenticationPrincipal Usuario usuario) {

        contato.setUsuarioId(usuario.getId());
        Contato contatoSalvo = contatoRepository.save(contato);
        return contatoSalvo;
    }

    public ContatosDTO listarContatosPorUsuario(@AuthenticationPrincipal Usuario usuario) {
        List<Contato> contatos = contatoRepository.findAllByUsuarioId(usuario.getId());
        return montaDTO(contatos, usuario);
    }

    public ContatosDTO montaDTO (List<Contato> contatos, Usuario usuario) {
        List<Telefone> telefones = new ArrayList<>();
        ContatosDTO contatosDTO = new ContatosDTO();
        for (Contato contato: contatos) {
            Telefone telefone = new Telefone();
            telefone.setNumeroTelefone(contato.getNumeroTelefone());
            telefones.add(telefone);
        }
        contatosDTO.setUsuario(usuario.getName());
        contatosDTO.setTelefones(telefones);

        return contatosDTO;
    }
}
